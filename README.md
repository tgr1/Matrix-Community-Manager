# Matrix Community Manager

[Matrix Community Manager Docs](https://sleuth56.gitlab.io/Matrix-Community-Manager/)

[Matrix Room](https://matrix.to/#/#Matrix-Community-Manager:matrix.org)

Matrix Community Manager or MCM is a chat bot written in Python using the [Opsdroid](github.com/opsdroid/opsdroid) chat-ops framework. It's designed to work with [Matrix](https://matrix.org) based chat applications.

MCM is easy to setup and use. It uses a standard Matrix account so it can be connected to public Matrix servers. You do not need to run your own home server. The Ansible playbook will get you up and running in minutes.

MCM is extremely customizable. Most aspects of this bot can be mostly customized using the matrix interface. Additional features can be customized in the configuration.toml file. MCM is great for leaders of communities, talk shows, Q&As, paging systems or anywhere else accepting and sorting messages would be helpful. 

To prevent miss use and spam. MCM only listens to the admin account. Adding filters, announcements, or other admins can be done in matrix! Type `!help` to get started.

## Major Features

- aggregate messages from rooms using mentions
- unlimited input and output rooms
- message sorting based on filters
- scheduled messages
- auto replies based on key words or regex
- mostly configurable from Matrix
- basic Access controls
- control who can invite the bot to new rooms
