from datetime import datetime
import pytz
import os
import toml
import re
import markdown

import logging
_LOGGER = logging.getLogger(__name__)

# Relocate python into the skill file's directory
__location__ = os.path.realpath(
  os.path.join(os.getcwd(), os.path.dirname(__file__)))
# Path to configuration.toml
configFilePath = os.path.join(__location__, 'configuration.toml')
# Nick. Populated at startup
mxid = None
# Regex to activate the bot when RequireMention is true. Populated at startup
mentionRegex = ""
# Path to configuration.toml. Populated at startup
skillRootConfigFile = ""
# help Banner. Populated at startup
helpBanner = ""
# Control Center
controlCenterRoomID = ""
# Code used to format time
timeCode = '%a %d %b %Y %H %M'


# ---------------
#
# Useful Regexes
#
#----------------
regex_matrixto='''<a href=["|']https://matrix.to/#/(.*)["|']>(.*)</a>'''
regex_mxid='''@(.*):(.*)'''
regex_roomid='''!(.*):(.*)'''

try:
   config = open(configFilePath, 'r')
   configFile = toml.loads(config.read())
finally:
   config.close()

# Loading all data from the configuration.toml file
def LoadConfigFile():
  global configFile
  # Load configFile
  with open(configFilePath, 'r') as config:
      configFile = toml.loads(config.read())

  # Prevents key errores
  if (not 'AutoReply' in configFile):
    configFile['AutoReply'] = {}
  if (not 'Announcement' in configFile):
    configFile['Announcement'] = {}
  if (not 'Filter' in configFile):
    configFile['Filter'] = {}

# Generates the fancy html message output
def formatResponse(user_name, message, room_id=None):
  messageFormat = configFile['General']['OutPutFormat']
  message = re.sub(mentionRegex, '', message)
  message = re.sub('^ \* ', '', message)
  message = markdown.markdown(message).replace("\n", "</br>")

  if (room_id != None):
    room_id = f"<a href=https://matrix.to/#/{room_id}>room</a>"
    return messageFormat.format(user_name = user_name, message = message, from_room = room_id)
  else:
    return messageFormat.format(user_name = user_name, message = message, from_room = "")

# Returns a list from the date given
def parseTimeToList(time):
  return time.replace(',', '').replace(':', ' ').split(' ')

# Returns true if the times given are the same. This includes fuzzy chars
def isItTimeYet(time: str):
  currentTime = datetime.now(pytz.timezone(configFile['General']['TimeZone']))
  currentTime = datetime.strftime(currentTime, timeCode).split(' ')
  time = parseTimeToList(time)

  for i in range(len(currentTime)):
    if (currentTime[i] != time[i]):
      if (time[i] != '*'):
        return False
  return True

# Returns true if the user in question is on the admins list
def isAdmin(username, configFile):
  keys = list(configFile['Admins'].keys())
  for i in range(len(configFile['Admins'])):
    if (username == configFile['Admins'][keys[i]]):
      return True
  return False

# Writes a python dict to the config file in toml format
def writeToConfig(configFilePath, contents):
  configFile = None
  with open(configFilePath, 'w') as config:
    toml.dump(contents, config)

# Returns true if all required fields are not none
def requirementsCheck(dictionary, requirements):
  for i in range(len(requirements)):
    if (dictionary[requirements[i]] == None):
      return False
  return True
