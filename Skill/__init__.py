# Opsdroid
from opsdroid.connector.matrix import ConnectorMatrix
from opsdroid.events import OpsdroidStarted, Message, EditedMessage, UserInvite, JoinRoom, Reaction
from opsdroid.matchers import match_event, match_regex, match_crontab, match_catchall
from opsdroid.skill import Skill

# Timed Message requirements
from datetime import datetime
import pytz

# Config file requirements
import toml

# Parsing
import markdown
import re

# Logging
import logging
_LOGGER = logging.getLogger(__name__)

# Internal dependencies
from . import commands
from . import resources

class Community_Manager_Skill(Skill):
  def __init__(self, opsdroid, config):
    super().__init__(opsdroid, config)

  # Runs at startup
  @match_event(OpsdroidStarted)
  async def onStart(self, opsdroid, config, event):
    # Test and load configs from configuration.toml
    if (self.config['path'][0] != '/'):
      _LOGGER.info("Matrix configuration requires a full path to be specified for the skill in configuration.yaml. If you want this feature you must resolve this and restart the bot.")
    else:
      resources.configFilePath = f"{self.config['path']}configuration.toml"
    resources.LoadConfigFile()

    self.editableMessages = {}

    # Populate resource variables
    resources.mxid = opsdroid.config['connectors']['matrix']['mxid']
    resources.mentionRegex = f"(^|[ * ]){str(opsdroid.config['connectors']['matrix']['nick'])}(?:[:\s]|$)"
    resources.skillRootConfigFile = f"{config['path']}configuration.toml"
    resources.helpBanner = f"{resources.configFile['General']['HelpBanner']}"
    resources.controlCenterRoomID = opsdroid.config['connectors']['matrix']['rooms']['main']

  # Auto join all rooms
  @match_event(UserInvite)
  async def respondToInvites(self, opsdroid, config, invite):
    _LOGGER.info(f"Got room invite for {invite.target}.")
    # If the auto join feature is enabled
    if (resources.configFile['Features']['AutoJoin']):
      # Join only admin invites
      if (resources.configFile['AutoJoin']['JoinOnlyAdminInvites']):
        if (resources.isAdmin(invite.user_id, resources.configFile)):
          _LOGGER.info(f"Joining invite from Admin.")
          await invite.respond(JoinRoom())
          _LOGGER.info(f"joined {invite.target}")
          return
        else:
          _LOGGER.info("Not an admin")
      # Join invites from users on listed servers
      if (resources.configFile['AutoJoin']['OnlyInvitesFromTrustedServers']):
        for server in resources.configFile['AutoJoin']['TrustedServers']:
          listedServer = resources.configFile['AutoJoin']['TrustedServers']
          if (re.match(resources.regex_roomid, invite.target).group(2) == server):
            _LOGGER.info(f"Joining invite from a user on a listed server.")
            await invite.respond(JoinRoom())
            _LOGGER.info(f"joined {invite.target}")
            return
        _LOGGER.info("Invite from user on non listed server")
      # Only join rooms originating from a listed server
      if (resources.configFile['AutoJoin']['OnlyRoomsOnTrustedServers']):
        for server in resources.configFile['AutoJoin']['TrustedServers']:
          listedServer = resources.configFile['AutoJoin']['TrustedServers']
          if (re.match(resources.regex_mxid, invite.user_id).group(2) == server):
            _LOGGER.info(f"Joining invite from a room on a listed server.")
            await invite.respond(JoinRoom())
            _LOGGER.info(f"joined {invite.target}")
            return
        _LOGGER.info("Invite from room not originating from listed server")
      if (not resources.configFile['AutoJoin']['JoinOnlyAdminInvites'] and not resources.configFile['AutoJoin']['OnlyInvitesFromTrustedServers'] and not resources.configFile['AutoJoin']['OnlyRoomsOnTrustedServers']):
        _LOGGER.debug(f"Joining room from invite.")
        await invite.respond(JoinRoom())
        _LOGGER.info(f"joined {invite.target}")
      else:
        _LOGGER.info("We will not be joining this room")

  # Timed Announcements
  @match_crontab('* * * * *', timezone="UTC")
  async def autoMessageHandler(self, event):
    currentTime = datetime.now(pytz.timezone(resources.configFile['General']['TimeZone']))
    announcements = resources.configFile['Announcement']

    for count in range(len(announcements)):
      currentAnnouncement = announcements[list(announcements.keys())[count]]

      # Check if the current event should fired
      if (resources.isItTimeYet(currentAnnouncement['Time'])):
        if (type(currentAnnouncement['RoomID']) == str):
          currentAnnouncement['RoomID'] = [currentAnnouncement['RoomID']]
        for room in currentAnnouncement['RoomID']:
          _LOGGER.info(f"Timed Message: {currentAnnouncement['Message']} {room} {currentTime.strftime(resources.timeCode)}")
          await self.opsdroid.send(Message(currentAnnouncement['Message'], target=room))

  # Command Line Interface
  # score_factor makes this function go first.
  @match_regex(r'^!', case_sensitive=False, score_factor=0.7)
  async def commands(self, opsdroid, config, message):
    if (re.search('!apply', message.text)):
      resources.LoadConfigFile()
      _LOGGER.info("Reloading config...")
      await message.respond("Reloaded config!")
      return

    await commands.command_helper(opsdroid, config, resources.configFile, message)
    resources.LoadConfigFile()

  # Filters
  @match_regex(f"((.*){resources.configFile['FilterConfig']['FilterDelimiter']}[\w'-]*)")
  async def filterHandler(self, opsdroid, config, message):
    filters = re.findall(f"({resources.configFile['FilterConfig']['FilterDelimiter']}[\w'-]*)", message.text)
    _LOGGER.info(f"Identified potential hash filters: {filters} testing against possible filters...")
    _LOGGER.info(f"Possible filters are: {resources.configFile['Filter']}")

    # If there are no possible filters log and return
    if (len(resources.configFile['Filter']) == 0):
      _LOGGER.info("No possible filters. Dropping...")
      return

    # Look for mention
    if (resources.configFile['FilterConfig']['RequireMention']):
      _LOGGER.info("RequireMention is true. Testing for mention...")
      # Check if fallback may be required per matrix spec
      if "format" in message.raw_event['content']:
        if (message.raw_event['type'] == 'm.room.message' and message.raw_event['content']['format'] == 'org.matrix.custom.html'):
          mentions = re.findall(resources.regex_matrixto, message.raw_event['content']['formatted_body'])

          wasFound = False
          for mention in mentions:
            if (mention[0] == resources.mxid):
              _LOGGER.info("Mention found. Continueing...")
              wasFound = True
              break
            _LOGGER.info("Mention not found. Returning...")
            return
      else:
        _LOGGER.info("Mention not found. Returning...")
        return

    # Loop though all found filters
    for foundFilter in filters:
      # Loop through possible tags
      for possibleFilter in resources.configFile['Filter']:
        tag = resources.configFile['Filter'][possibleFilter]['Filter']
        # Test if the filters match
        if (foundFilter.lower() == f"{resources.configFile['FilterConfig']['FilterDelimiter']}{tag.lower()}"):
          _LOGGER.info(f"filter: {foundFilter} matched possible filter: {tag.lower()}")

          noTagString = re.sub(f"{foundFilter.lower()}", "", message.text)
          submitter = message.user
          tagSentToRoomID = resources.configFile['Filter'][possibleFilter]['RoomID']
          tagResponseRoomID = message.target

          # Checks if this message was an edit
          if ("m.relates_to" in message.raw_event['content']):
            try:
              translationInfo = self.editableMessages[message.raw_event['content']['m.relates_to']['event_id']]
            except:
              _LOGGER.info("Original message not found")
              return
            _LOGGER.info(message.raw_event['content']['m.relates_to']['event_id'])
            _LOGGER.info(translationInfo)
            data = EditedMessage(text=resources.formatResponse(submitter, noTagString, tagResponseRoomID), linked_event=translationInfo['event_id'], target=translationInfo['room_id'])
            _LOGGER.info(vars(data))
            await opsdroid.send(data)
            await opsdroid.send(Message(resources.configFile['Messages']['SuccessResponse'].format(user_name=submitter), target=tagResponseRoomID))
          # Send new message
          else:
            origEvent = await opsdroid.send(Message(resources.formatResponse(submitter, noTagString, tagResponseRoomID), target=tagSentToRoomID))
            await self.opsdroid.send(Message(resources.configFile['Messages']['SuccessResponse'].format(user_name = message.user), target=message.target))
            #                              User message ID                 Receiving room     Bot message ID 
            self.editableMessages[message.raw_event['event_id']] = {'room_id': origEvent.room_id, 'event_id': origEvent.event_id}
        else:
          await self.processMessage(opsdroid, config, message)


  @match_catchall(messages_only=True)
  async def processMessage(self, opsdroid, config, message):
    _LOGGER.info('processMessage')
    string = message.raw_event['content']['body']
    _LOGGER.info('User Message: ' + str(string))

    # Check if this is a piped room
    pipeSourceRooms = []
    for pipe in resources.configFile['Pipe']:
      pipeSourceRooms.append(resources.configFile['Pipe'][pipe]['Source'])

    if (message.target in pipeSourceRooms):
      await self.opsdroid.send(Message(text=resources.formatResponse(message.user, re.sub(resources.mentionRegex, '', string), message.target), target=resources.configFile['Pipe'][pipe]['Destination']))
      return

    if (re.search(resources.mentionRegex, string) or resources.configFile['General']['RequireMention'] == False):
      keys = list(resources.configFile['AutoReply'].keys())

      # See if we have a predetermined response
      if (len(resources.configFile['AutoReply']) > 0):
        for i in range(len(keys)):
          _LOGGER.info(re.sub(resources.mentionRegex, '', string))

          if (re.search(resources.configFile['AutoReply'][keys[i]]['Regex'], re.sub(resources.mentionRegex, '', string).lstrip(), re.IGNORECASE)):
            if (resources.configFile['AutoReply'][keys[i]].get('RoomID', False)):
              await self.opsdroid.send(Message(resources.configFile['AutoReply'][keys[i]]['Message'].format(user_name=message.user), target=resources.configFile['AutoReply'][keys[i]]['RoomID']))
            else:
              await message.respond(resources.configFile['AutoReply'][keys[i]]['Message'].format(user_name=message.user))
            return

      # Checks if this message was an edit
      if ("m.relates_to" in message.raw_event['content']):
        try:
          translationInfo = self.editableMessages[message.raw_event['content']['m.relates_to']['event_id']]
        except:
          _LOGGER.info("Original message not found")
          return
        _LOGGER.info(message.raw_event['content']['m.relates_to']['event_id'])
        _LOGGER.info(translationInfo)
        data = EditedMessage(text=resources.formatResponse(message.user, re.sub(resources.mentionRegex, '', string), message.target), linked_event=translationInfo['event_id'], target=translationInfo['room_id'])
        _LOGGER.info(vars(data))
        await self.opsdroid.send(data)
        await self.opsdroid.send(Message(resources.configFile['Messages']['SuccessResponse'].format(user_name = message.user), target=message.target))
      # Send new message
      else:
        origEvent = await self.opsdroid.send(Message(resources.formatResponse(message.user, string, message.target), target=self.config.get('main')))
        await self.opsdroid.send(Message(resources.configFile['Messages']['SuccessResponse'].format(user_name = message.user), target=message.target))
        #                              User message ID                 Receiving room     Bot message ID 
        self.editableMessages[message.raw_event['event_id']] = {'room_id': origEvent.room_id, 'event_id': origEvent.event_id}

  # Prevents memory leak over time from event IDs being stored permanently
  # A nice change would be to only purge old IDs. An easy improvement to make :)
  @match_crontab('0 4 * * *', timezone=resources.configFile['General']['TimeZone'])
  async def cleanDatabase(self, event):
    self.editableMessages = {}
    _LOGGER.info("Cleaned editableMessages dictionary")

  # @match_regex(r'hi')
  # async def reaction(self, opsdroid, config, message):
  #   EMOJI_YES = '\u2705'
  #   EMOJI_NO = '\u274c'
  #   EMOJI_THUMBSUP = '\U0001f44d\ufe0f'
  #   matrix_connector = self.opsdroid.get_connector("matrix")
  #   _LOGGER.info("hi")
  #   # resp = await matrix_connector.send(Message("hi"))
  #   self.confirm_event = await matrix_connector._event_creator.create_event_from_eventid(message.event_id,
  #                                                                                            message.target)
  #   await matrix_connector.react(self.confirm_event, EMOJI_NO)

  # @match_event(Reaction)
  # async def reaction(self, event):
  #   import emoji
  #   EMOJI_YES = emoji.emojize(":check_mark_button:")
  #   EMOJI_ROBOT = emoji.emojize(":robot:")
  #   if (event.emoji == EMOJI_YES):
  #     await self.opsdroid.send(Message("HI"))