# Ansible Installation Instructions

This playbook installs matrix community manager automatically inside a docker container.

## Supported Operating Systems

* Ubuntu 20.04 and 22.04 LTS
* Fedora 35
* 
* More coming soon

## Pre-Run Requirements

* A server running a supported OS
* A matrix account and it's password
* A dedicated matrix room with the bot's account already invited to it

## Configuring The Playbook

Below is an example hosts file. It's important to note that if you run this playbook over an existing install it will overwrite the main configuration file that stores all admins, announcements, filters, etc. So you should back that file up or be prepared to re-input them via the matrix commands.

If your are making changes to an existing installation it's recommended to edit the configuration files manually. They are located at `/matrix-community-manger/`. The bot's configuration.toml file is inside the `Skill/` directory.

file name: hosts
```yml
all:
  hosts:
    0.0.0.0:
      ansible_user: root
  vars: 
    pip_package: python3-pip
    pip_install_packages:
      - name: docker
      - name: psycopg2-binary
  
    docker_users:
      - root
    docker_install_compose: true
    docker_version: latest
  
    # Matrix Community Manager config vars
    mxid: "@mcm:example.com"
    password: ""
    admin_room: "!internalid:example.com"
    homeserver: "https://matrix.example.com"
    nick: "MCM"
  
    admin_mxid: '@admin:example.com'
    time_zone: 'US/Eastern'
    require_mention: 'true'
  
    success_response: '{user_name} I sent your message!'
    output_format: '<h4>{from_room} {user_name} Said</h4><blockquote><p>{message}</p></blockquote>'
  
    # Features
    auto_join: 'true'
    JoinOnlyAdminInvites: 'true'
    OnlyInvitesFromTrustedServers: 'false'
    OnlyRoomsOnTrustedServers: 'false'
    TrustedServers: []

    # Filters
    filter_delimiter: '#'
    filter_sucess_response: '{user_name} I sent your message with {filter}!'
    filter_require_mention: 'false'
```

## Running The Playbook

### Tags

* all: Used for initial setup. Runs all steps to setup and start the bot.
* fix: Fixes encryption errors by obtaining a new device ID and deleting the old key cache.
* config: Rewrite config files.
* start: Start the bot.
* stop: Stop the bot.

Run the playbook with this command
```bash
ansible-galaxy install -r requirements.yml
ansible-playbook -i hosts -t TAGS playbook.yml
```