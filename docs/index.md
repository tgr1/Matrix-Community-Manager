# Change Notes

## 1.02 Feature Release

### Features

* Adding a filter now uses the room the command was sent from by default if the roomid is not specified.
* Ansible role now has tags for start, stop, configs, fix and all.
* Ansible role backs up the configuration.toml file before it does anything.
* Add support for ansible core :crossed_finger:

### Docs

* Update ansible install doc with tag descriptions.

## 1.01 Feature Beta

### Features

* Ansible playbook to automate deployments
* You can how change the delimiter for filtered messages
* Edits to posts
* Removed email style DMs. This feature isn't useful anymore given that the bot can already handle when you edit messages. Bots can still be configured to allow DMs by setting the `AutoJoin` to true.
* Auto join can be configured to only join invites from admins, rooms originating on certain homeservers, or users from certain homeservers.
* Announcements can be sent to several rooms at a time.
* Add from room link to output messages.

### Bug fixes

* Some text characters were causing crashes
* Normal messages were getting classed as filter messages 
* Global require mention shouldn't be used for filters

### Docs

* Installation with Ansible
* Change the index page to show change notes
