# Manual Installation

This installation guide will be using a [Docker](https://docker.com) container. Docker is the recommended way of running this bot.
This guide assumes that you already have access to a Linux host running docker and have docker compose installed.

## Cloning the repository

First we will use [Git](https://git-scm.com/) to download the repository for MCM.
```bash
git clone https://gitlab.com/Sleuth56/Matrix-Community-Manager
cd Matrix-Community-Manager
```

After cloning the repository we have to put our Matrix account's credentials in the configuration file.
You should create the Matrix account you want to use for the bot now. You can do that with whatever Matrix client you want.

Below is an example configuration file. In here you must change the `MXID, password and homeserver.`

```yml
welcome-message: false

connectors:
  matrix:
    mxid: "@mcm:example.com"
    password: "" # Put the password for the bot account here.
    rooms:
      'main': '!RoomID:example.com' #This room will be the control room for the bot.
    homeserver: "https://matrix.example.com"
    nick: "Matrix-Community-Manager"  # The nick name for the bot. This can be whatever you want.
    store_path: "/home/opsdroid/.config/opsdroid/Matrix_cache/" # You don't need to change this.

    # Encryption optional
    # enable_encryption: True
    # device_name: "MCM"
    # device_id: ""

skills:
  Matrix-Community-Manager:
    # When running in a container you don't need to change this.
    path: /home/opsdroid/.config/opsdroid/Skill/

databases:
  sqlite:
     path: "/home/opsdroid/.config/opsdroid/messages.db"
```

While we are customizing our configuration file we should setup the main room. This room is where the bot will display messages it doesn't know what to do with and error messages. This should be a private room used only to control the bot. Any room can be used to control the bot even private messages to it. The admin interface is only accessible to admins no matter what room they come from. 

* Create a room using your Matrix account.
* Invite the bot user to this room. You will get an error when the bot starts if it can not join this room.
* Open room settings and go to advanced. Under room information your looking for `Internal room ID`. It looks like `!junk:yourhomeserver.com`
* Replace `!RoomID:example.com` with what you copied from element.

```yml
    rooms:
      'main': '!RoomID:example.com' #This room will be the control room for the bot.
```

## Enabling Encryption (Optional, but recommended)

To enable encryption you need to obtain a device ID. Matrix uses End To End Encryption. That means to send an encrypted message your device has to encrypt the message for each other device in a room. Matrix Community Manager connects to an account just like your normal Matrix client does. This means that it also has a device ID. There are 3 ways of obtaining/generating a device ID.

### 1. Using curl to login to the bots Matrix account.

Replace `example.com` with your Matrix server's address.

#### Resolving .well-known issues

If your Matrix server uses .well-known which many do. Than we will need to find out what that url is. Put this into your web browser. Changing `example.com` to be your homeserver's address is.
```bash
https://example.com/.well-known/matrix/client
```

You should get back a json object with the url for the actual Matrix server. Put that URL in the curl command below.

```json
{"m.homeserver": {"base_url": "https://matrix.example.com"}}
```

#### Obtaining a new device ID.

In this case our user is `mcm` if you used a different Matrix account in your config you will have to change that here as well. You will also have to provide the account's password.

```bash
    curl -X POST "https://example.com/_matrix/client/v3/login" \                                      ✔ 
         -H "Accept: application/json" \
         -H "Content-Type: application/json" \
         -d '{"identifier":{"type":"m.id.user","user":"mcm"},"initial_device_display_name":"mcm","password":"","type":"m.login.password"}' 
```
```bash
    {"user_id":"@mcm:example.com","access_token":"REDACTED","home_server":"example.com","device_id":"REDACTED"}%
```

### 2. Synapse Admin Interface
Use the synapse admin interface to look at all devices associated with the bot's account. This is outside of the scope of this document. However if you own your own Matrix server than this is the easiest way.

### 3. Login With An Other Matrix Client
Login using an other Matrix client to the bot's Matrix account. In element you can go to the devices panel under settings and see other devices. (This is not recommended)

#### Enabling Encryption In Your Configuration File.

Uncomment these lines from the example above. Paste in the device ID you got from the steps above. If you used a different device name change that here to.
```yml
    Encryption optional
    enable_encryption: True
    device_name: "MCM"
    device_id: ""
```

## Granting Admin Rights

Now it's time to make you the admin. Later we can use the Matrix interface to add more admins but sense you need to be an admin to access that we have to add the first one manually.

You need to add a key in the `configuration.toml` file. Change directory into the git repository we cloned earlier.

```bash
nano Skill/configuration.toml
```

You are looking for `[Admins]` Paste the example below into the `configuration.toml` file changing it to match your Matrix account ID.

```toml
YourName = @yourMatrixID:example.com
```

## Verifying That Your Bot Is Working

Now that we are finished configuring the bot all we need to do is start it.

```bash
sudo docker-compose up -d
```

That's it. As long as there is no errors your instance of Matrix Community Manager is up and running. You should verify that your instance is working correctly. Below is instructions on how to do that.

Example output for a properly configured instance.
```bash
opsdroid_1  | INFO opsdroid.logging: ========================================
opsdroid_1  | INFO opsdroid.logging: Started opsdroid 0+unknown.
opsdroid_1  | INFO opsdroid.core: Opsdroid is now running, press ctrl+c to exit.
opsdroid_1  | INFO opsdroid.database.sqlite: Connected to sqlite /home/opsdroid/.config/opsdroid/messages.db
```

You can check your log using.

```bash
sudo docker-compose logs opsdroid
```

## Example Room Layout
The bot should have joined the room listed in the configuration file.

Now that you have verified that your bot instance is working properly without errors we can setup some rooms. If your happy setting up rooms on your own than you can stop reading here. Below are some suggestions of how you might want to create rooms for your bot. The bot does not require this configuration these are just what I have found useful for my own deployments.

* Main space (Generally the one already used by your community)
    * MCM SubSpace (Private)
        * Bot Control Room (This is the one in the configuration file)
        * Command Spam Room
        * A mirror of your front room (Great for testing automated announcements and making sure that the bot works as you expected)
        * Add rooms for filters to sort messages into as you see fit.

See the [Destination command]() to setup backrooms.